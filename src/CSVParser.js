const papaParse = require('papaparse');
const fs = require('fs');
const path = require('path');

const keys = [];

function extractHeader(headerArray) {
  headerArray.forEach((head, i) => {
    const key = head.replaceAll('/', '')
      .replaceAll('&', '')
      .replaceAll('#', '')
      .replaceAll('XD', '')
      .replaceAll(' ', '_')
      .replaceAll('__', '_')
      .replaceAll(';', '')
      .toLowerCase();
    keys[i] = key;
  })
}

function extractBody(dayContent) {
  let JSON = {};

  dayContent.forEach((body, i) => {
    if (i < dayContent.length) JSON[keys[i]] = body.replaceAll('&', '')
    .replaceAll('#', '')
    .replaceAll('XD', '')
    .replaceAll('xd', '')
    .replaceAll(';', '')
    .trim();
    else JSON = {};
  });

  return JSON;
}

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

module.exports = function readAndParseCSV(fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(__dirname, `../csv/${fileName}`), 'utf8', function (err, contents) {
      if (err) {
        reject(err);
      }
      const parsedContent = papaParse.parse(contents);
      const allFGTimesheets = [];

      if (parsedContent.errors.length === 0) {
        const timesheetData = parsedContent.data;

        timesheetData.forEach((timesheet, i) => {
          if (i === 0) {
            // header
            extractHeader(timesheet);
          } else {
            // body
            allFGTimesheets.push(extractBody(timesheet));
          }
        })
      }

      resolve(allFGTimesheets);
    });
  });
}
