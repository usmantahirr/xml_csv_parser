const path = require('path');
const fs = require('fs');
const xml2js = require('xml2js');

module.exports = function parseXML(fileName) {
  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(__dirname, `../xml/${fileName}`), 'utf8', function (err, contents) {
      if (err) reject(err);
      xml2js.parseString(contents, (err, data) => {
        if (err) reject(err);
        resolve(data['soapenv:Envelope']['soapenv:Body'][0]['ns1:downloadResult'][0]['_']);
      });
    });
  });
}
