const readXML = require('./src/XMLParser');
const readAndReplace = require('./src/CSVParser');
const fs = require('fs');

readXML('response.xml').then((csvString) => {
  fs.writeFile(
    './csv/response.csv',
    csvString.trim(),
    'utf8',
    (err) => {
      if (err) console.log(err);
      else {
        console.log('CSV Written');

        readAndReplace('response.csv').then((jsonData) => {
          fs.writeFile(
            './json/response.json',
            JSON.stringify(jsonData),
            'utf8',
            (err) => {
              if (err) console.log(err);
              else {
                console.log('JSON Written');
              }
            }
          );
        }).catch(err => {
          console.log('CSV -> JSON Error', err);
        });
      }
    }
  );
}).catch((err) => {
  console.log('XML -> CSV Error', err);
});
